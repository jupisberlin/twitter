import sqlite3
import os

if(os.path.exists("tweet.db")):
	os.remove("tweet.db")

conn = sqlite3.connect('tweet.db')
c = conn.cursor()

c.execute('''CREATE TABLE pub (autor text, tweetID text, threadID int, time text, content text, send text)''')
c.execute('''CREATE TABLE private (autor text, kuerzel text, tweetID text, threadID int, time text, content text, send text, task text)''')

conn.commit()
c.close()
