# -*- coding: utf-8 -*-

from twitter import twitter
import sqlite3
import time
import sys

class tpublic(twitter):
	def __init__(self, consumer_key, consumer_secret, access_token, access_token_secret):
		twitter.__init__(self, consumer_key, consumer_secret, access_token, access_token_secret)
		self.conn = sqlite3.connect('tweet.db')
		self.c = self.conn.cursor()
		#self.c.execute("UPDATE private SET send='False' WHERE threadID = 3")
		#self.conn.commit()
	def saveMentions(self):
		m = self.getMentions()
		for mention in m:
			#äprint str(mention.id)+": "+mention.text + " by " + mention.user.screen_name
			self.c.execute("SELECT * FROM pub WHERE tweetID = '"+str(mention.id)+"'")
			if self.c.fetchone() == None:
				#äprint "SAVE"
				#True
				thread = self.getnewThreadID()
				self.c.execute("INSERT INTO pub (autor, tweetID, threadID, time, content, send) VALUES ('"+mention.user.screen_name+"', '"+str(mention.id)+"', "+str(thread)+", '"+str(time.time())+"', '"+mention.text+"', 'False')")
				self.conn.commit()
	def sendTweets(self, private):
		print "SEND"
		d = self.conn.cursor()

		for row in self.c.execute("SELECT * FROM private WHERE send = 'False' AND task = 'send'"):
			d.execute("UPDATE private SET send='True' WHERE threadID = "+str(row[3]))
			print row
			tweet = row[5]+" ^"+row[1]
			if(len(tweet)>140):
				print "To LONG"
				#private.sendDM("Your Message "+str(row[3])+" was to long!", str(row[0]))
			else:
				self.sendTweet(tweet)
				print tweet
		self.conn.commit()
	def retweet(self):
		for row in self.c.execute("SELECT * FROM private WHERE send = 'False' AND task = 'rt'"):
			d.execute("UPDATE private SET send='True' WHERE threadID = "+str(row[3]))
			response = self.c.execute("SELECT * FROM pub WHERE threadID = "+str(int(row[5])))
			reso = self.c.fetchone()
			self.retweet(reso[1])
			
	def answerTweets(self, private):
		print "Answer"
		d = self.conn.cursor()
		for row in self.c.execute("SELECT * FROM private WHERE send = 'False' AND task != 'send'"):
			print row
			d.execute("UPDATE private SET send='True' WHERE threadID = "+str(row[3]))
			try:
				response = self.c.execute("SELECT * FROM pub WHERE threadID = "+str(int(row[7])))
				reso = self.c.fetchone()

				if(response!= None):
					print row
					print reso
					tweet = "@"+reso[0]+" "+row[5]+" ^"+row[1]
					if(len(tweet)>140):
						print "To LONG"
						#private.sendDM("Your Message "+str(row[3])+" was to long!", str(row[0]))
					else:
						print reso[1]
						self.sendTweet(tweet, reso[1])
						print tweet
			except:
				print "Unexpected error:", sys.exc_info()
				#print row[7]
		self.conn.commit()
	def getnewThreadID(self):
		self.c.execute("SELECT threadID FROM pub ORDER BY threadID DESC LIMIT 0, 1")
		res = self.c.fetchone()
		if res == None:
			return 1
		else:
			print res[0]
			return int(res[0])+1
			

